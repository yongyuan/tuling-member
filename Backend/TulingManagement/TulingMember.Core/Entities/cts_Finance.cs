﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    [TableAudit]
    public class cts_Finance: DEntityTenant
    {
    
        /// <summary>
        /// 流水号。
        /// </summary>
     
        public string OrderNo { get; set; }
        /// <summary>
        /// 流水号。
        /// </summary>

        public DateTime? OrderDate { get; set; }


        /// <summary>
        /// 。
        /// </summary>

        public decimal FinanceAmount { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long FinanceItemId { get; set; }


        /// <summary>
        /// 。
        /// </summary> 
        public string FinanceItem { get; set; }

    
        /// <summary>
        /// 。1.收入2.支出
        /// </summary>
     
        public int FinanceItemType { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string Remark { get; set; }

    }
}