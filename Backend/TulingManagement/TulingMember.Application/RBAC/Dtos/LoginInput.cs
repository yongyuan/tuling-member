﻿using System.ComponentModel.DataAnnotations;

namespace TulingMember.Application
{
    /// <summary>
    /// 登录输入参数
    /// </summary>
    public class LoginInput
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "用户名不能为空")]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
    }
    public class UpdatePwdInput
    {       

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "原密码不能为空")]
        public string OldPassword { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
       
    }
    public class ResetPwdInput
    {

        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "手机号不能为空")]
        public string Account { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        [Required(ErrorMessage = "验证码不能为空")]
        public string Code { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }

    }
}