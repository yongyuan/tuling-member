﻿using Furion;
using Furion.DatabaseAccessor;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Furion.UnifyResult;
using JoyAdmin.Core;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Application.Dto;
using TulingMember.Core; 
using Yitter.IdGenerator;

namespace TulingMember.Application
{
    public class SupplierService : IDynamicApiController
    {
        private readonly ILogger _logger; 
        private readonly IRepository<cts_Supplier> _supplierRepository;
        private readonly IRepository<cts_SupplierPayLog> _supplierpaylogRepository;
        public SupplierService(ILogger logger 
            , IRepository<cts_Supplier> supplierRepository
            , IRepository<cts_SupplierPayLog> supplierpaylogRepository)
        {
            _logger = logger;
            _supplierRepository = supplierRepository;
            _supplierpaylogRepository = supplierpaylogRepository;
        }

        
        #region 供应商管理
        /// <summary>
        /// 列表
        /// </summary> 
        public PagedList<cts_Supplier> SearchSupplier(BaseInput input)
        {
            var search = _supplierRepository.AsQueryable(); 
            if (!string.IsNullOrEmpty(input.keyword))
            {
                search = search.Where(m => m.Name.Contains(input.keyword)
                || m.Phone.Contains(input.keyword)  );
            }
            var amount = search.Sum(m=>m.Balance);
            UnifyContext.Fill(new
            {
                Balance = amount,
            });
            return search.ToPagedList(input.page, input.size);
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [SecurityDefine("supplier")]
        [UnitOfWork]
        public void SaveSupplier(cts_Supplier input)
        {
            var oldCount= _supplierRepository.Where(m=>m.Name.Equals(input.Name)&&m.Id!=input.Id).Count();
            if (oldCount>0)
            {
                throw Oops.Bah("名称已存在，请检查").StatusCode(ErrorStatus.ValidationFaild);
            }
            if (input.Id > 0)
            {
                _supplierRepository.UpdateExclude(input,new string[] { nameof(input.Balance) });
            }
            else {
                input.Id=YitIdHelper.NextId();
                _supplierRepository.Insert(input);
                
                if (input.Balance!=0)
                {
                    _supplierpaylogRepository.Insert(new cts_SupplierPayLog
                    {
                        SupplierId=input.Id,
                        SupplierName=input.Name, 
                        ChangeAmount=input.Balance, 
                        NewAmount=input.Balance,  
                        OrderDate=DateTime.Now.Date,
                        Remark="期初账目"
                    });
                }
            } 
            
        }

        /// <summary>
        /// 获取 
        /// </summary>  
        public cts_Supplier GetSupplier(long id)
        {
           return  _supplierRepository.FindOrDefault(id);
        }
        /// <summary>
        /// 删除 
        /// </summary>  
        [SecurityDefine("supplier")]
        public void DeleteSupplier(long id)
        {
            _supplierRepository.FakeDelete(id);
        }

        /// <summary>
        /// 客户账目明细
        /// </summary> 
        public PagedList<cts_SupplierPayLog> SearchSupplierPayLog(BaseInput input)
        {
            var search = _supplierpaylogRepository.Where(m=>m.SupplierId==input.supplierid);
            if (input.sdate != null)
            {
                search = search.Where(m => m.CreatedTime >= input.sdate);
            }
            if (input.edate != null)
            {
                var edate = input.edate?.AddDays(1).Date;
                search = search.Where(m => m.CreatedTime < edate);
            }
            return search.OrderByDescending(m=>m.OrderDate).ThenByDescending(m=>m.Id).ToPagedList(input.page, input.size);
        }
        /// <summary>
        /// 新增一条账目明细
        /// </summary> 
        [SecurityDefine("supplier")]
        [UnitOfWork]
        public void AddSupplierPayLog(cts_SupplierPayLog input)
        {
            var supplier = _supplierRepository.FindOrDefault(input.SupplierId);
            var changeAmount = input.ChangeAmount + input.DiscountAmount;
            input.OldAmount = supplier.Balance;
            input.NewAmount = supplier.Balance + changeAmount;
            supplier.Balance += changeAmount;
            _supplierpaylogRepository.Insert(input); 
        }
        #endregion



    }
}
